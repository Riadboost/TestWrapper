<?php


namespace ParkingMap;


use Parkingmap\Wrapper\Item\ItemInterface;

class Item implements ItemInterface
{

    private string $ref;
    private array $services = [];
    private bool $isUpdated = false;

    public function __construct(string $documentId, string $ref, array $itemData)
    {
        foreach ($itemData as $field => $value) {
            $this->$field = $value;
        }
        $this->documentId = $documentId;
        $this->ref = $ref;
        if (!is_array($this->services)) {
            trigger_error("services is not correctly defined " . print_r($this->services, true), E_USER_WARNING);
            $this->services = [];
        }
    }

    public function getDocumentId(): string
    {
        return $this->documentId;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    public function getData(): array
    {
        return (array)$this;
    }

    public function getServicesDefinitions(): array
    {
        return $this->services;
    }

    public function getIsUpdated(): bool
    {
        return $this->isUpdated;
    }

    public function getProperty(string $propertyKey) {

        if(property_exists($this, "properties") && array_key_exists($propertyKey, $this->properties)) {
            return $this->properties[$propertyKey];
        }

        return null;
    }

    public function setProperty(string $propertyKey, $propertyValue): void {

        if(property_exists($this, "properties")) {
            $this->properties[$propertyKey] = $propertyValue;
        } else {
            $this->properties = [
                $propertyKey => $propertyValue
            ];
        }

        $this->isUpdated = true;
    }

    public function toAssociativeArray(): array
    {
        $content = get_object_vars($this);
        unset($content['_id']);
        unset($content["ref"]);
        unset($content["isUpdated"]);
        unset($content["documentId"]);
        return $content;
    }
};