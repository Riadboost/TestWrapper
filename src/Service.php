<?php

namespace ParkingMap;

use Parkingmap\Wrapper\Event\EventInterface;
use Parkingmap\Wrapper\Event\Events;
use Parkingmap\Wrapper\Item\ItemInterface;
use Parkingmap\Wrapper\Service\ServiceInterface;
use Parkingmap\Wrapper\Store\DataStoreInterface;

class Service implements ServiceInterface
{
    protected array $config;
    protected DataStoreInterface $datastore;

    /**
     * @param  EventInterface  $event
     * @param  ItemInterface  $item
     * @return Events
     */
    public function run(EventInterface $event, ItemInterface $item): Events
    {
        return (new Events())->add($event);
    }

    public function __construct(array $config, ?DataStoreInterface $dataStore)
    {
        //datastore is not used in this king of service
        if ($dataStore !== null)
            $this->datastore = $dataStore;
        $this->config = $config;
    }
};
