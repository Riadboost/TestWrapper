# THE TEST #

We want yout to implement the following workflow:

In the car park "PM-1", a sensor (also known as Box) with the id "512" is set up to detect movements on 2 different parking spots.
Every time the sensor detects an occupancy change, it sends a JSON string as an event.

Find bellow the event definition and the items that will be used in this workflow.

## Event 
- Box 
```json
{
    "1" : 0, # 0 is Free
    "2" : 1, # 1 is busy
}
```

## Items 
- Box 512
```json
{
    "id":"512",
    "name":"512",
    "spots": {
        "1":"1", # The id of place is linked to Spot with id 1
        "2":"32"
    }
}
```
- Spot
```json
{
    "id":"1",
    "name":"spot 1",
    "sectionId" : 1,
    "maxtime" : 180,
    "properties" : {
        "status" : 0
    }
}
```
- Section
```json
{
    "id":"1",
    "name":"Section PMR PM-1",
    "address" : "in the end of flat earth",
    "properties" : {
        "MaxPlaces" : 2,
        "FreePlaces" : 1
    }
}
```

Every time an event is received, the "status" property of every Spot items as well as the "freePlaces" property of the Section item should be updated.

## Event

To send an event to an item, a routing key in the format [ItemType].id.[ItemId] is required. 

```php
new Event('spot.id.1',new DateTime(),[...data])
```


# WorkFlow

The following services need to be implemented:
- BoxSplitService : Reads the event and sends an event to each Spot item that should be updated
- SpotUpdateState : Reads the event and updates the property "status" of the current Item then sends an event to the Section item
- SectionUpdateFreePlace : Reads the event and updates the property "freePlaces"

Every services must return a list of events event if it's empty.