<?php

require_once(__DIR__ . '/../vendor/autoload.php');

use PHPUnit\Framework\TestCase;
use ParkingMap\Loader;
use ParkingMap\Services\BoxSplitService;

class TestWorkflow extends TestCase
{

    public function testBox()
    {
        $event = Loader::loadEventBox("512");
        $box = Loader::loadItemBox("512");
        // Run 1 stage of event
        $events = (new BoxSplitService([], null))->run($event, $box);
        $this->assertCount(2, $events);

        // Run 2 stage of event
        foreach ($events as $event) {
            $events2run = (new SpotUpdateState([], null))->run($event, Loader::loadItemSpot($event->getContent()['spotId']));
            $this->assertCount(1, $events2run);
            // Run 3 stage of event
            foreach ($events2run as $event) {
                $events3run = (new SectionUpdateFreePlace([], null))->run($event, Loader::loadItemSection($event->getContent()['sectionId']));
                $this->assertCount(0, $events3run);
            }
        }
    }
}
