<?php

require_once(__DIR__ . '/../vendor/autoload.php');

use PHPUnit\Framework\TestCase;
use ParkingMap\Hello;
use ParkingMap\Item;
use Parkingmap\Wrapper\Event\Event;

class TestHello extends TestCase
{
    public function testHello()
    {
        $event = new Event("box.512", new DateTime(), ['spots' => ['1' => 0, '2' => 1]]);
        $item = new Item("1", "1", ['properties' => ['msg' => '']]);
        (new Hello([], null))->run($event, $item);
        $this->assertTrue($item->getProperty('msg') === 'world');
    }
};
