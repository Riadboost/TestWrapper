# Test Wrapper Parking Map #

See [TEST.md](TEST.md) to find the subject of the test.

### Requirements
This test needs Docker to run. (see https://docs.docker.com/get-docker/)

### How to use ###

Run config.run in bash

```shell
./config.run
```

### Run Hello Test

Execute this command to run the Hello Test
```shell
./test.run
```

### Run workflux Test

```shell
./test.run Workflow
```
